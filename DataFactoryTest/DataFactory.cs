﻿using Microsoft.Azure.Management.DataFactory;
using Microsoft.Azure.Management.DataFactory.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.Rest.Serialization;
using System;
using System.Collections.Generic;

namespace DataFactoryTest
{
    public class DataFactory
    {
        private AzureConfig config;

        public DataFactory(AzureConfig config)
        {
            this.config = config;
        }

        public void BulkUpdate()
        {
            var client = CreateDataManagementClient();

            //CreateDataFactory(client);

            CreateOracleDbLinkedService(client);
            CreateCosmosDbLinkedService(client);

            CreateOracleDbDataset(client);
            CreateCosmosDbDataset(client);

            CreatePipelineWithCopyActivity(client);
            CreateRunResponse result = RunPipeline(client);
            MonitorPipelineRun(client, result.RunId);

            //DeleteDataFactory(client);
        }

        private DataFactoryManagementClient CreateDataManagementClient()
        {
            var context = new AuthenticationContext("https://login.windows.net/" + config.TenantID);
            ClientCredential cc = new ClientCredential(config.ApplicationId, config.AuthenticationKey);
            AuthenticationResult result = context.AcquireTokenAsync("https://management.azure.com/", cc).Result;
            ServiceClientCredentials cred = new TokenCredentials(result.AccessToken);

            return new DataFactoryManagementClient(cred)
            {
                SubscriptionId = config.SubscriptionId
            };
        }

        private void CreateDataFactory(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating data factory " + config.DataFactoryName + "...");

            var dataFactory = new Factory
            {
                Location = config.Region,
                Identity = new FactoryIdentity()
            };

            client.Factories.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, dataFactory);

            Console.WriteLine(SafeJsonConvert.SerializeObject(dataFactory, client.SerializationSettings));

            while (client.Factories.Get(config.ResourceGroup, config.DataFactoryName).ProvisioningState == "PendingCreation")
            {
                System.Threading.Thread.Sleep(1000);
            }
        }

        private void CreateOracleDbLinkedService(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating Oracle linked service " + config.OracleDbLinkedServiceName + "...");

            var storageLinkedService = new LinkedServiceResource(
                new OracleLinkedService
                {
                    ConnectionString = new SecureString(
                        "User Id=system;Password=password;" +
                        "Host=40.123.38.238;Port=1521;" +
                        "Sid=XE;"),
                    ConnectVia = new IntegrationRuntimeReference
                    {
                        ReferenceName = config.IntergationRuntimeName
                    }
                }
            );
            client.LinkedServices.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, config.OracleDbLinkedServiceName, storageLinkedService);

            Console.WriteLine(SafeJsonConvert.SerializeObject(storageLinkedService, client.SerializationSettings));
        }

        private void CreateCosmosDbLinkedService(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating Cosmos linked service " + config.CosmosDbLinkedServiceName + "...");

            LinkedServiceResource cosmosDbLinkedService = new LinkedServiceResource(
                new CosmosDbLinkedService
                {
                    // cosmos db -> settings -> keys -> primary connection string
                    ConnectionString = new SecureString(
                        "AccountEndpoint=https://cosmos-rapid.documents.azure.com:443/;AccountKey=TjlCEdomIhlCbTyktzVFijW0e7HQzOXVx6uf0G4A2sSfeaE1GtRX9bLLzVDJqHTgJRVuakqgQ98SmNofvsSM9A==;Database=ToDoList"),
                }
            );

            client.LinkedServices.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, config.CosmosDbLinkedServiceName, cosmosDbLinkedService);
            Console.WriteLine(SafeJsonConvert.SerializeObject(cosmosDbLinkedService, client.SerializationSettings));
        }

        private void CreateOracleDbDataset(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating Oracle dataset " + config.OracleDbDatasetName + "...");

            var oracleDbDataset = new DatasetResource(
                new OracleTableDataset
                {
                    LinkedServiceName = new LinkedServiceReference
                    {
                        ReferenceName = config.OracleDbLinkedServiceName
                    },
                    Schema = new object[] { },
                    OracleTableDatasetSchema = "OT",
                    Table = "COUNTRIES"
                }
            );

            client.Datasets.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, config.OracleDbDatasetName, oracleDbDataset);

            Console.WriteLine(SafeJsonConvert.SerializeObject(oracleDbDataset, client.SerializationSettings));
        }

        private void CreateCosmosDbDataset(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating Cosmos dataset " + config.CosmosDbDatasetName + "...");

            var cosmosDbDataset = new DatasetResource(
                new DocumentDbCollectionDataset
                {
                    LinkedServiceName = new LinkedServiceReference
                    {
                        ReferenceName = config.CosmosDbLinkedServiceName
                    },
                    CollectionName = config.CosmosDBCollection
                }
            );

            client.Datasets.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, config.CosmosDbDatasetName, cosmosDbDataset);

            Console.WriteLine(SafeJsonConvert.SerializeObject(cosmosDbDataset, client.SerializationSettings));
        }

        private void CreatePipelineWithCopyActivity(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating pipeline " + config.PipelineName + "...");

            var pipeline = new PipelineResource
            {
                Activities = new List<Activity>
                {
                    new CopyActivity
                    {
                        Name = "CopyFromOracleToCosmosDB",
                        Inputs = new List<DatasetReference>
                        {
                            new DatasetReference()
                            {
                                ReferenceName = config.OracleDbDatasetName
                            }
                        },
                        Outputs = new List<DatasetReference>
                        {
                            new DatasetReference
                            {
                                ReferenceName = config.CosmosDbDatasetName
                            }
                        },
                        Source = new OracleSource { OracleReaderQuery = "SELECT * FROM OT.countries" },
                        Sink = new DocumentDbCollectionSink { }
                    }
                }
            };

            client.Pipelines.CreateOrUpdate(config.ResourceGroup, config.DataFactoryName, config.PipelineName, pipeline);

            Console.WriteLine(SafeJsonConvert.SerializeObject(pipeline, client.SerializationSettings));
        }

        private CreateRunResponse RunPipeline(DataFactoryManagementClient client)
        {
            Console.WriteLine("Creating Pipeline run...");

            CreateRunResponse runResponse =
                client.Pipelines.CreateRunWithHttpMessagesAsync(config.ResourceGroup, config.DataFactoryName, config.PipelineName).Result.Body;

            Console.WriteLine("Pipeline run ID: " + runResponse.RunId);

            return runResponse;
        }

        private void MonitorPipelineRun(DataFactoryManagementClient client, string pipelineRunId)
        {
            Console.WriteLine("Checking Pipeline Run Status...");

            PipelineRun pipelineRun;
            while (true)
            {
                pipelineRun = client.PipelineRuns.Get(config.ResourceGroup, config.DataFactoryName, pipelineRunId);
                Console.WriteLine("Status: " + pipelineRun.Status);

                if (pipelineRun.Status == "InProgress")
                {
                    System.Threading.Thread.Sleep(15000);
                }
                else
                {
                    break;
                }
            }
        }

        private void DeleteDataFactory(DataFactoryManagementClient client)
        {
            Console.WriteLine("Deleting the data factory");

            client.Factories.Delete(config.ResourceGroup, config.DataFactoryName);
        }
    }
}
