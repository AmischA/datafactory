﻿using Microsoft.Azure.Management.Logic;

namespace DataFactoryTest
{
    /**
     * Actions to create resources via SDK:
     * 1. Create a resource, e.g. data factory, on Azure
     * 2. Assign permissions for the app registered within AD in Subscriptions (so from code we can create Azure resources)
     * */
    class Program
    {
        // Key for intergation runtime
        // IR@3990d519-e821-4de5-95dc-8fddb23dac13@Rapid-factory@eu@Bv6u1Fk1DtQ/+ouxNJ2ValZUB7+DgaYo6IMteEthxK4=

        static void Main(string[] args)
        {
            //var dataFactory = new DataFactory(AzureConfig.Instance);
            //dataFactory.BulkUpdate();

            var logicApp = new LogicApp(AzureConfig.Instance);
            logicApp.SetupIncrementalUpdate();
        }
    }
}
