﻿namespace DataFactoryTest
{
    public class AzureConfig
    {
        private static AzureConfig instance;

        public string TenantID => "290dbe96-b076-46bc-b333-261e04ab9f14";

        // Subscriptions -> ID
        public string SubscriptionId => "2226f68d-9e9a-4c92-9443-1093dd894164";

        // register an app within AD (AD -> app registration) -> application/client ID
        public string ApplicationId => "69b8db07-10fe-4714-9b39-f81f88d06d08";

        // key created during app registration withing AD (app registration -> certificates & secrets -> new client secret)
        public string AuthenticationKey => "RhNy30DD=AA[Kj2hzEV.AL.ibV4RqZ==";

        // Resource group name
        public string ResourceGroup => "ora";

        // Location of the resource group
        public string Region => "East US 2";

        public string DataFactoryName => "Rapid-factory";

        //static string storageAccount = "<your storage account name to copy data>";
        //static string storageKey = "<your storage account key>";

        //Oracle and Cosmos DBs linked services and the pipeline
        public string OracleDbLinkedServiceName => "OracleDbLinkedService";
        public string OracleDbDatasetName => "OracleDataset";
        public string CosmosDbLinkedServiceName => "CosmosDbLinkedService";
        public string CosmosDbDatasetName => "CosmosDbDataset";
        public string PipelineName => "RapidPipeline";

        //azure cosmos DB collections name (container name)
        public string CosmosDBCollection => "Items";

        // integration runtime name
        public string IntergationRuntimeName => "RapidIntegrationRuntime";

        public static AzureConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AzureConfig();
                }

                return instance;
            }
        }
    }
}
