﻿using Microsoft.Azure.Management.Logic;
using Microsoft.Azure.Management.Logic.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using Microsoft.Rest.Azure;
using Microsoft.Rest.Azure.OData;
using Newtonsoft.Json.Linq;
using System;

namespace DataFactoryTest
{
    public class LogicApp
    {
        private AzureConfig config;

        public LogicApp(AzureConfig config)
        {
            this.config = config;
        }

        public void SetupIncrementalUpdate()
        {
            var client = CreateLogicAppClient();
            var workflow = CreateWorkflow();

            var logicAppId = Guid.NewGuid().ToString();

            string workflowName = RunWorkflow(client, workflow, logicAppId);

            Console.WriteLine("Is workflow successful: " + (workflowName == logicAppId));

            var filter = new ODataQuery<WorkflowRunFilter> { Top = 25 };
            IPage<WorkflowRun> allWorkflows = client.WorkflowRuns.List(config.ResourceGroup, workflowName, filter);
        }

        private LogicManagementClient CreateLogicAppClient()
        {
            var context = new AuthenticationContext("https://login.windows.net/" + config.TenantID);
            ClientCredential cc = new ClientCredential(config.ApplicationId, config.AuthenticationKey);
            AuthenticationResult result = context.AcquireTokenAsync("https://management.azure.com/", cc).Result;
            ServiceClientCredentials cred = new TokenCredentials(result.AccessToken);

            return new LogicManagementClient(cred)
            {
                SubscriptionId = config.SubscriptionId
            };
        }

        private Workflow CreateWorkflow()
        {
            Console.WriteLine("Creating workflow ...");

            return new Workflow
            {
                State = WorkflowState.Enabled,
                Location = config.Region,
                Definition = JToken.Parse(GetWorkflowDefinition())
            };
        }

        private string GetWorkflowDefinition()
        {
            return @"{ 
                '$schema':'http://schema.management.azure.com/providers/Microsoft.Logic/schemas/2016-06-01/workflowdefinition.json#', 
                'contentVersion':'1.0.0.0', 
                'triggers': { 
                    'Recurrence': { 
                        'recurrence': { 
                            'frequency': 'Hour', 
                            'interval': 1 
                        }, 
                        'type': 'Recurrence' 
                    } 
                }, 
                'actions': { 
                    'ping_microsoft_site': { 
                        'type': 'Http', 
                        'inputs':{                                 
                            'method':'GET', 
                            'userAgent':'LogicApp', 
                            'uri':'https://www.microsoft.com/en-in/' 
                        } 
                    } 
                }     
            }";
        }

        private string RunWorkflow(LogicManagementClient client, Workflow workflow, string logicAppId)
        {
            Console.WriteLine("Running workflow ...");       

            var resultWorkflow =
                    client.Workflows.CreateOrUpdateAsync(config.ResourceGroup, logicAppId,
                        workflow).Result;

            return resultWorkflow.Name;
        }
    }
}
